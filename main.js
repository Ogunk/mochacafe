/**
 * Declaring all the listeners
*/
var dealBanner = null;
var responsiveMenuButton = null;
var responsiveMenuCloseButton = null;

/*
    Adding the listeners when the DOM is loaded
*/
window.addEventListener("DOMContentLoaded", () => {

    /*
        Adding listener to close the deal banner
    */
    dealBanner = document.getElementsByClassName("close-deal-button")[0];
    if (dealBanner) {
        dealBanner.addEventListener("click", handleCloseDealBannerClick, false);
    } else {
        console.error("Cannot load deal closer listener");
    }

    /*
        Adding listener to the menu button on small devices
    */
    responsiveMenuButton = document.getElementsByClassName("open-menu-icon")[0];
    if (responsiveMenuButton) {
        responsiveMenuButton.addEventListener("click", handleOpenResponsiveMenuClick, false);
    } else {
        console.error("Cannot load responsive menu button");
    }

    /**
     * Adding listener to the close button on small devices
     */
    responsiveMenuCloseButton = document.getElementById("responsive-menu-close-button");
    if (responsiveMenuCloseButton) {
        responsiveMenuCloseButton.addEventListener("click", handleCloseResponsiveMenuClick, false);
    } else {
        console.error("Cannot load responsive close menu button");
    }
})

/*
    Closing the menu if user resize the window
*/
window.addEventListener("resize", () => {
    let responsiveMenu = document.getElementsByClassName("responsive-menu-list")[0];
    responsiveMenu.style.display = "none";
})

/*
    Function to close the deal banner
*/

function handleCloseDealBannerClick() {
    if (dealBanner) {
        let topbarOffre = document.getElementsByClassName("topbar-offre")[0];
        topbarOffre.style.display = "none";
        dealBanner.style.display = "none";
    }
}

/*
    Function to open of close the responsive menu
*/
function handleOpenResponsiveMenuClick() {
    if (responsiveMenuButton) {
        let responsiveMenu = document.getElementsByClassName("responsive-menu-list")[0];
        responsiveMenu.style.display = "flex";
    }
}

/**
 * Function to close the responsive menu
 */
function handleCloseResponsiveMenuClick() {
    if (responsiveMenuCloseButton) {
        let responsiveMenu = document.getElementsByClassName("responsive-menu-list")[0];
        responsiveMenu.style.display = "none";
    }
}